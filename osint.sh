#!/bin/bash

usage="usage: \n./osint.sh <domain name> <outputfile.txt>"

#require two arguments
if [ $# -lt 2 ]; then
    echo -e $usage
    exit 1
fi

#check if root
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

echo

install_dnstwist () {

#check if dnstwist is installed

        check_dnstwist=$(pip list | grep dnstwist)
        if [ "" = "$check_dnstwist" ]
            then
                echo "DNSTwist not installed, installing...."  
                        pip install dnstwist
            else
                echo "DNSTwist installed"
                echo
        fi
}

install_dorkin () {

#check if dorkin is installed

        check_dorkin=$(ls /opt/ | grep dorkin)
        if [ "" = "$check_dorkin" ]     
            then
                echo "dorkin not installed, installing...."
                git clone https://github.com/IvanGlinkin/Fast-Google-Dorks-Scan.git /opt/dorkin
                chmod +x /opt/dorkin/FGDS.sh
            else
                echo "dorkin installed"
                echo
        fi
}

#call functions to install
install_dnstwist
install_dorkin

#run dnstwist
echo
echo "DNSTwist" > $2
echo "Running DNSTwist"
echo
dnstwist --registered $1 | sed -e '1,8d' |sed 's/^.\{11\}//' >> $2

#run amass
echo "amass" >> $2
echo "Running amass"
echo
chmod +x amass
./amass enum -brute -src -ip -min-for-recursive=3 -d $1 -o temp.txt >/dev/null
cat temp.txt | sed 's/^.\{18\}//' >> $2
rm temp.txt

#run dorkin
echo "dorkin" >> $2
echo "Running the dorks"
echo
/opt/dorkin/FGDS.sh $1 | tee dorks_$1
